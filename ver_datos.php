<?php
  include "MVC/Controller/get_datos.php";
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Noelia Castillo i Jonathan Raya">
    <meta name="description" content="Refugi per a animals">
    <link rel="stylesheet" href="css/all.min.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700|Ubuntu&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <title>Resultados del formulario</title>
</head>
<body>
<header class="site-header site-header-table">
        <div class="bar contenedor">            
            <a class="logo" href="index.html"><i class="fas fa-paw"></i></a>
            <nav class="navigation-bar">
                <a href="index.html">Inicio</a>
                <a href="#">Adopción</a>
                <a href="#">Sobre nosotros</a>
                <a href="#">Blog</a>
                <a href="#">Contacto</a>
                <a href="formulario.html">Formulario</a>
                <div class="social-media">
                    <a class="twitter" href="#"><i class="fab fa-twitter-square"></i></a>
                    <a class="instagram" href="#"><i class="fab fa-instagram"></i></a>
                    <a class="youtube" href="#"><i class="fab fa-youtube"></i></a>    
                </div>
            </nav>                              
        </div>       
        <div class="welcome contenedor">
            <h1>Resultats de formulari</h1>
        </div>      
    </header>

    <main class="table-box box-card">
        <p class="table-title">Tabla de registros</p>
        <!-- Crida a la funcio que crea la taula -->
        <?php create_table(); ?> 
    </main>
    
    <footer>
        <div class="footer-info contenedor">
            <div>
                <p>Adoptamigos. Todos los derechos reservados.&copy; 2019-2020</p>
            </div>
            <div class="social-media">
                <a class="twitter" href="#"><i class="fab fa-twitter-square"></i></a>
                <a class="instagram" href="#"><i class="fab fa-instagram"></i></a>
                <a class="youtube" href="#"><i class="fab fa-youtube"></i></a>            
            </div>
        </div>
    </footer>
</body>
</html>