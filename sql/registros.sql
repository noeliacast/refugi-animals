-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Temps de generació: 20-01-2020 a les 14:42:33
-- Versió del servidor: 10.1.41-MariaDB-0+deb9u1
-- Versió de PHP: 7.2.25-1+0~20191128.32+debian9~1.gbp108445

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `formulario`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `datos_formulario`
--

CREATE TABLE `datos_formulario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_bin NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_bin NOT NULL,
  `email` varchar(50) COLLATE utf8_bin NOT NULL,
  `telefono` varchar(12) COLLATE utf8_bin DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `ciudad` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `atractivo` tinyint(2) DEFAULT NULL,
  `conocido` varchar(30) COLLATE utf8_bin DEFAULT NULL,
  `mejora_inicio` tinyint(1) DEFAULT NULL,
  `mejora_formulario` tinyint(1) DEFAULT NULL,
  `mejora_fotos` tinyint(1) DEFAULT NULL,
  `mejora_trabajadores` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Bolcament de dades per a la taula `datos_formulario`
--

INSERT INTO `datos_formulario` (`id`, `nombre`, `apellidos`, `email`, `telefono`, `fecha_nacimiento`, `ciudad`, `atractivo`, `conocido`, `mejora_inicio`, `mejora_formulario`, `mejora_fotos`, `mejora_trabajadores`) VALUES
(7, 'Noelia', 'Castillo', 'castillonoelia98@gmail.com', '666666666', '1998-11-09', 'Caldes de Montbui', 10, 'Friend', 0, 0, 0, 0),
(8, 'Jonathan', 'Raya Rios', 'jonathan17699@hotmail.es', '666666666', '1999-06-17', 'Tarrassa', 10, 'Other', 0, 0, 0, 0),
(9, 'Mari', 'Contreras', 'mcr63palau@gmail.com', '77777777', '1963-09-23', 'Palau de Plegamans', 9, 'Friend', 0, 0, 0, 0),
(10, 'Antonio', 'Castillo', 'antoniocastilloruiz@gmail.com', '640888759', '1961-07-07', 'Palau de Plegamans', 10, 'Friend', 0, 0, 0, 0),
(11, 'Marc', 'Montroig Rios', 'mmontroig1997@gmail.com', '601038862', '1997-05-23', 'Barcelona', 9, 'Friend', 0, 0, 0, 0),
(12, 'Paula', 'Borrego', 'paulabgago@gmail.com', '654318763', '1998-09-30', 'Barcelona', 8, 'Friend', 1, 0, 0, 1),
(13, 'Cristina', 'Sicre Flores', 'sicre.f@hotmail.com', '666666666', '1992-08-01', 'Tarrassa', 10, 'Friend', 0, 1, 0, 0),
(14, 'Luis', 'Dominguez', 'luizdominguez@gmail.com', '685124584', '2000-11-30', 'Caldes de Montbui', 8, 'Google', 0, 0, 0, 1),
(15, 'Barbara', 'Rios Zaldivar', 'barbara100476@gmail.com', '666666666', '1976-04-10', 'Sabadell', 10, 'Friend', 0, 0, 0, 0),
(16, 'Francisco', 'Raya Carmona', 'f.raya@gmail.com', '666666666', '1968-12-18', 'Sabadell', 10, 'Ad', 0, 0, 0, 1),
(17, 'Emilio', 'Raya Carmona', 'emilioraya@gmail.com', '666666666', '1977-10-14', 'Sabadell', 6, 'Other', 1, 0, 0, 1),
(18, 'Michelle', 'Indalecio', 'mindalecio@gmail.com', '62542581', '1997-12-31', 'Mollet del Valles', 10, 'Other', 0, 0, 0, 0),
(19, 'Marc', 'Gómez', 'gomirtex@gmail.com', '62542581', '1997-12-11', 'Palau', 9, 'Other', 0, 0, 0, 0),
(20, 'Emma', 'Sáez', 'emmasaez@gmail.com', '651324894', '2001-12-08', 'Barcelona ', 9, 'Friend', 0, 0, 0, 0),
(21, 'Natalia', 'Diaz', 'nataliadiaz@gmail.com', '6666666666', '1998-06-07', 'Barcelona', 7, 'Other', 0, 0, 0, 0),
(22, 'Laia', 'Martín ', 'laiamartin@gmail.com', '625325123', '1998-02-14', 'Barcelona ', 8, 'Other', 0, 0, 0, 1),
(23, 'Maria', 'Giménez', 'mgimenez@gmail.com', '600412354', '1980-04-10', 'Barcelona', 9, 'Google', 0, 0, 0, 0),
(29, 'Judit', 'Mateo Morales', 'jmateo@gmail.com', '640077895', '1998-01-28', 'Palau', 9, 'Friend', 0, 0, 0, 0),
(30, 'Raquel', 'Castillo', 'castilloraquel91@gmail.com', '681254365', '1991-07-17', 'Barcelona', 9, 'Friend', 0, 0, 0, 1);

--
-- Índexs per a les taules bolcades
--

--
-- Índexs per a la taula `datos_formulario`
--
ALTER TABLE `datos_formulario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `datos_formulario`
--
ALTER TABLE `datos_formulario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
