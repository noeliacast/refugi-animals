<?php

function create_table() {

    // Conexio a la base de dades.
    $connection = mysqli_connect("localhost", "jonathan", "super3", "formulario");

    if (!$connection)
    {
        echo "Error: No se pudo conectar a MySQL.";
        exit();
    }

    //Generem la consulta
    $sql = "SELECT * FROM datos_formulario";
    mysqli_set_charset($connection, "utf8"); //formato de datos utf8

    if(!$result = mysqli_query($connection, $sql)) die();

    // Creacio de les capçaleres de la taula
    $table = "<table> <tr> <th>Id</th> <th>Nombre</th> <th>Apellidos</th> <th>Email</th> <th>Teléfono</th> <th>Fecha de nacimiento</th> <th>Ciudad</th> <th>Atractivo</th> <th>Cómo nos ha conocido</th> <th>Mejorar homepage</th> <th>Mejorar formulario</th> <th>Mejorar fotos</th> <th>Mejorar sección de trabajadores</th> </tr>";

    $counter = 0;
    $sumBeauty = 0;

    $array = array();

    // Bucle per llegir tots els registres de la taula.
    while($row = mysqli_fetch_array($result))
    {
        $id = $row["id"];
        $name = $row["nombre"];
        $surname = $row["apellidos"];
        $email = $row["email"];
        $telf = $row["telefono"];
        $birth_date = $row["fecha_nacimiento"];
        $city = $row["ciudad"];
        $beauty = $row["atractivo"];
        $know_us = $row["conocido"];
        $upgrade_home = $row["mejora_inicio"];
        $upgrade_form = $row["mejora_formulario"];
        $upgrade_pictures = $row["mejora_fotos"];
        $upgrade_workers = $row["mejora_trabajadores"];

        $sumBeauty = $beauty + $sumBeauty;

        // Afegir un 1 al valor de beauty que ha llegit
        $array[$beauty - 1] += 1;

        // Registres amb colors intercalats
        if ($counter % 2 == 0)
        {
            $table = $table . "<tr class =\"tr1\"><td>" . $id . "</td>";
            $table = $table . "<td>" . $name . "</td>";
            $table = $table . "<td>" . $surname . "</td>";
            $table = $table . "<td>" . $email . "</td>";
            $table = $table . "<td>" . $telf . "</td>";
            $table = $table . "<td>" . $birth_date . "</td>";
            $table = $table . "<td>" . $city . "</td>";
            $table = $table . "<td>" . $beauty . "</td>";
            $table = $table . "<td>" . $know_us . "</td>";
            $table = $table . "<td>" . $upgrade_home . "</td>";
            $table = $table . "<td>" . $upgrade_form . "</td>";
            $table = $table . "<td>" . $upgrade_pictures . "</td>";
            $table = $table . "<td>" . $upgrade_workers . "</td></tr>";
        }
        else
        {
            $table = $table . "<tr class =\"tr2\"><td>" . $id . "</td>";
            $table = $table . "<td>" . $name . "</td>";
            $table = $table . "<td>" . $surname . "</td>";
            $table = $table . "<td>" . $email . "</td>";
            $table = $table . "<td>" . $telf . "</td>";
            $table = $table . "<td>" . $birth_date . "</td>";
            $table = $table . "<td>" . $city . "</td>";
            $table = $table . "<td>" . $beauty . "</td>";
            $table = $table . "<td>" . $know_us . "</td>";
            $table = $table . "<td>" . $upgrade_home . "</td>";
            $table = $table . "<td>" . $upgrade_form . "</td>";
            $table = $table . "<td>" . $upgrade_pictures . "</td>";
            $table = $table . "<td>" . $upgrade_workers . "</td></tr>";
        }

        $counter += 1;
    }

    $table = $table . "</table>";

    $media = $sumBeauty / $counter;
    // Trunca els decimals i deixa nomes els 2 ultims.
    $media = number_format($media, 2);
    echo $table; 
    echo "</br>";
    echo "<p>Media atractivo: " . $media . "<p>";

    // Aconseguir l'ndex amb el valor maxim
    $maxs = array_keys($array, max($array));

    echo "<p>Moda atractivo: " . ($maxs[0] + 1) . "<p>";
    mysqli_close($connection);
}

?>