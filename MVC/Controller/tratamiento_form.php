<?php

	// Conexio a la base de dades
	$connection = mysqli_connect("localhost", "jonathan", "super3", "formulario");

	if (!$connection)
	{
		echo "Error: No se pudo conectar a MySQL.";
		exit();
	}

	// Recuperacio del registres
	$name = $_POST["name"];
	$surname = $_POST["surname"];
	$email = $_POST["email"];
	$telf = $_POST["telf"];
	$birth_date = $_POST["birth_date"];
	$city = $_POST["city"];
	$beauty = $_POST["beauty"];
	$know_us = $_POST["know-us"];
	$upgrades = $_POST["upgrade"];

	// Codi pero veure les dades que recull el formulari

	// echo "Nombre: " . $name . "</br>";
	// echo "Apellidos: " . $surname . "</br>";
	// echo "Email: " . $email . "</br>";
	// echo "Telefono: " . $telf . "</br>";
	// echo "Fecha de nacimiento: " . $birth_date . "</br>";
	// echo "Ciudad: " . $city . "</br>";
	// echo "Atractivo: " . $beauty . "</br>";
	// echo "Conocido: " . $know_us . "</br>";
	// print_r($upgrades);

	$query =  mysqli_prepare($connection,"INSERT INTO datos_formulario (nombre, apellidos, email, telefono, fecha_nacimiento, ciudad, atractivo, conocido, mejora_inicio, mejora_formulario, mejora_fotos, mejora_trabajadores) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)");
	mysqli_set_charset($connection, "utf8");

	// Convertir el valor dels Checkbox a booleans per al MySQL

	$homepage;
	$formpage;
	$pictures;
	$workers;
	if (in_array("homepage", $upgrades))
	{
		$homepage = 1;
	}
	else
	{
		$homepage = 0;
	}

	if (in_array("formpage", $upgrades))
	{
		$formpage = 1;
	}
	else
	{
		$formpage = 0;
	}

	if (in_array("pictures", $upgrades))
	{
		$pictures = 1;
	}
	else
	{
		$pictures = 0;
	}

	if (in_array("workers", $upgrades))
	{
		$workers = 1;
	}
	else
	{
		$workers = 0;
	}

	//Cal especificar els tipus de les dades que s'enviaran
	mysqli_stmt_bind_param ($query, "ssssssisiiii", $name,$surname, $email, $telf,$birth_date, $city, $beauty, $know_us, $homepage, $formpage, $pictures, $workers);
	mysqli_stmt_execute($query);
	
	//Redireccio a la pagina del formulari. Comentar aixo per veure els "Echo" del formulari.
	header('Location: /virtual381/uf1_JonathanRaya/AdoptaAmigos/formulario.html');
?>